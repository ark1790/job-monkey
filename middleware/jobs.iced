
mongoose = require "mongoose"
Jobs = mongoose.model "Jobs"


@fetchOne = (req , res , next)->
  {id} = req.params

  Jobs.findOne()
    .where("_id" : id)
    .exec (err , job)->
      if err?
        console.log err
        return req.status(404).send "NO SUCH JOB"

      req.job = job
      next()
