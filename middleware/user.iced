
mongoose = require "mongoose"

User = mongoose.model "User"

@bind =
    loggedIn : (req , res , next) ->
        if not req.isAuthenticated()
            req.flash "error" , "Login required!"
            return res.redirect "/user/login"
        next()

    isAdmin : (req , res , next) ->
        if not req.isAuthenticated()
            req.flash "error" , "Login required!"
            return res.redirect "/user/login"

        {user} = req

        if user.role is not 'admin'
            req.flash "error" , "Access Denied!"
            return res.redirect "back"

        next()

    fetchOne : (req , res , next)->
      {id} = req.params || req.body

      User.findById id , (err , user)->
        if err?
          console.log err
          return next(err)

        req._user = user
        next()
