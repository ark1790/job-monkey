express = require "express"
@router = express.Router()
passport = require "passport"

@controller = require("../controller/jobs")
@middleware = require("../middleware/jobs")
@user = require( "../middleware/user").bind


@router.route("/create")
  # .all(@user.loggedIn)
  .post(@controller.create)

@router.route("/create")
  # .all(@user.loggedIn)
  .get (req , res) ->
    res.render "jobs/create" , message : req.flash( "job-message")

@router.route("/update/:id")
  .all(@user.loggedIn)
  .all(@middleware.fetchOne)
  .get (req , res) ->
    {job} = req
    res.render "jobs/update" ,
      message : req.flash( "job-message")
      job : job

@router.route("/update/")
  .all(@user.loggedIn)
  .post(@controller.update)

@router.route("/find_job/")
  .get (req , res)->
    res.render "find_job" , 
      current_page : 'find_job'

@router.route("/list")
  .get @controller.list 

@router.route("/get_job_details/:id")
  .all(@middleware.fetchOne)
  .get (req , res)->
    console.log req.job
    res.send JSON.stringify req.job