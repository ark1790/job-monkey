express = require "express"
@router = express.Router()
passport = require "passport"
multer = require "multer"
upload = multer dest : "./uploads/user_images/"
User_skills = require("mongoose").model "User_skills"

_ = require "underscore"

@controller = require "../controller/user"
@middleware = require("../middleware/user").bind



@router.get "/login", @controller.getLogin

@router.post "/login" , passport.authenticate "local-login" , {
	successRedirect : "/user/profile"
	failureRedirect : "/user/login"
	failureFlash : true
}

@router.get "/signup" , @controller.getSignUp

@router.route("/signup")
	.all( upload.fields([
		name : "user_image"
		maxCount : 1
	]))
	.all( (req , res , next) ->
		console.log "YYYYYYYYYY"
		console.log req.body
		next()
	)
	.post  passport.authenticate "local-signup" , {
	successRedirect : "/user/profile"
	failureRedirect : "/user/login"
	failureFlash : true
}

@router.route("/update/:id")
	.all(@middleware.loggedIn)
	.all(@middleware.fetchOne)
	.get(@controller.getUpdate)

@router.route("/update")
	.all(@middleware.loggedIn)
	.post(@controller.postUpdate)

@router.route( "/profile" )
	.all(@middleware.loggedIn)
	.get(@controller.getProfile)

@router.route( "/skills/:id" )
	.all(@middleware.loggedIn)
	.all(@middleware.fetchOne)
	.get( (req,res)->
		user = req._user

		user.local = _.omit user.local , 'password'

		await User_skills.findOne().where("user_id" , user.id).exec defer err , skills
		if err?
			console.log "err"
			return res.status(500).send err

		res.render "skills" ,
			user : user
			user_data : JSON.stringify user
			user_skills: JSON.stringify skills
	)

@router.route( "/edit_profile" )
	.all(@middleware.loggedIn)
	.get( (req,res)->
			res.render "edit_profile",
				user : req.user
	)



@router.route("/delete/:id")
	.all(@middleware.isAdmin)
	.all(@middleware.fetchOne)
	.get(@controller.delete)

@router.route('/logout')
	.all(@middleware.loggedIn)
	.get @controller.getLogout


@router.route("/skills/add/:type")
	.post(@controller.addSkill)

@router.route("/skills/:id/:type/delete/:sid")
	.get(@controller.deleteSkill)


@router.route("/skills/:id/:type/update/:sid")
	.all((req , res , next)->
		console.log "YAYAYAYYAYA"
		next()
	)
	.post(@controller.updateSkill)
