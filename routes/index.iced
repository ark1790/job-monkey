

user = require "./user"
jobs = require "./jobs"


@bind = (app)->

    # Route Groups

    app.get "/" , (req , res)->
        res.status(200).render "index"

    app.use "/user/" , user.router

    app.use "/jobs/" , jobs.router
