
mongoose = require "mongoose"
Job = mongoose.model "Jobs"
_ = require "underscore"

@create = (req , res) ->
  
  console.log req.body
  data =  _.omit req.body , ['submit' , 'salary']

  data['salary'] = parseFloat(req.body.salary)

  console.log data

  job = new Job data

  job.save (err)->
    if err?
      console.log err
      return res.status(500).send err
    # req.flash "job-message" , "Successfully Created"
  
    res.send req.body


@update = (req , res)->
    job = _.omit req.body , ['submit' , 'id']
    {id} = req.body

    # console.log job

    Job.update {"_id" : id} , job , (err , jb)->
      if err?
        console.log err
        return res.send err

    req.flash "job-message" , "Successfully Updated!"
    res.redirect "back"

@lists = (req , res)->
    result = {}
    result.data=[]
    result.data[0]=[]
    result.data[0][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/077c42a0-f75d-49ec-be5b-ca77ffc53f9a/'>"
    result.data[0][1]="<a>Graduate Sales Consultant</a><br>Graduate Role in Software Sales."
    result.data[0][2]="IT & Telecommunications, Sales"
    result.data[0][3]="£20,000"
    result.data[0][4]="Dhaka"
    result.data[0][5]="31/01/2016"
    result.data[0][6]='<div class="margin-bottom-5"><button class="btn btn-sm btn-primary filter-submit margin-bottom"><i class="fa fa-search"></i>Save</button></div><button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>View Details</button>'
  
    result.data[1]=[]
    result.data[1][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/077c42a0-f75d-49ec-be5b-ca77ffc53f9a/'>"
    result.data[1][1]="<a>Graduate Sales Consultant</a><br>Graduate Role in Software Sales."
    result.data[1][2]="IT & Telecommunications, Sales"
    result.data[1][3]="£20,000"
    result.data[1][4]="Dhaka"
    result.data[1][5]="31/01/2016"
    result.data[1][6]='<div class="margin-bottom-5"><button class="btn btn-sm btn-primary filter-submit margin-bottom"><i class="fa fa-search"></i> Save</button></div><button class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i> View Details</button>'
    result.query=req.query
    res.send JSON.stringify result

 # { __v: 0,
 #    salary: 122020,
 #    details: '<ol><li>asdfasdfasdfasdfsdf</li><li>Fasd</li><li>fasdf</li><li>asdf</li><li>asdf</li></ol>',
 #    location: 'asdfasdf',
 #    summary: 'asdfasd',
 #    recruiter: 'aaaa',
 #    sector: 'Accounting & Finance',
 #    closing_date: Wed Feb 17 2016 06:00:00 GMT+0600 (Bangladesh Standard Time),
 #    title: 'adqad',
 #    _id: 56bf41fe80cb8a7c2363005e 
 #  } 




@list = (req , res) ->

  Job.find().exec (err , jobs)->
    if err?
      console.log err
      res.send err

    console.log "JOBS"
    console.log jobs

    result = {}
    result.data = []
    for job in jobs
      data = []
      data[0] = "<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/077c42a0-f75d-49ec-be5b-ca77ffc53f9a/'>"
      data[1] = job.sector
      data[2] = job.title
      data[3] = "$"+job.salary
      data[4] = job.location
      data[5] = job.closing_date
      data[6] = '<div class="margin-bottom-5"><button class="btn btn-sm btn-primary filter-submit margin-bottom"><i class="fa fa-search"></i> Save</button></div><button id="'+job.id+'"  class="btn btn-sm btn-default job_details_view"><i class="fa fa-times"></i> View Details</button>'

      result.data.push data

    res.send result
