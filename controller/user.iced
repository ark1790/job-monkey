
mongoose = require "mongoose"
User = mongoose.model "User"
User_skills = mongoose.model "User_skills"
_ = require "underscore"

@getIndex = (req , res) ->
	res.render "index"

@getLogin = (req , res) ->
	res.render "login" , message : req.flash "loginMessage"

@getSignUp = (req , res) ->
	res.render "signup" , message : req.flash "signupMessage"

@getProfile = (req , res)->
	console.log req.session
	res.render "profile" ,
		user : req.user

@getLogout = (req , res)->
	req.logout()
	res.redirect "/"

@getUpdate = (req , res)->
	{_user} = req
	if not _user
		return res.send "NO SUCH USER"
	res.status(200).render "user/update" , user : _user , message : req.flash "updateUserMessage"

@delete = (req , res)->
	{_user} = req

	_user.remove (err)->
		if err?
			console.log err
			return res.send err

		res.redirect "/"

@postUpdate= (req , res)->
	user = _.omit req.body , ['submit' , 'id']
	{id} = req.body

	User.update { "_id" : id } , user , (err , usr)->
		if err?
			console.log err
			return res.send err


		req.flash "updateUserMessage" , "YAY!"
		res.redirect "/user/update/#{id}"

@addSkill = (req , res)->
	data = _.omit req.body , ['submit' , '_id' , 'id']

	{type} = req.params

	console.log data
	User_skills.findOne().where("user_id" , req.body.id).exec (err , skills)->
		if err?
			console.log err
			return res.status(500).send err

		if not skills
			console.log "create new skill"
			skills = new User_skills()

		skills[type].push data
		skills['user_id'] = req.body.id

		skills.save (err)->
			if err?
				return res.status(500).send err

			if req.user._id == req.body.id
				req.session.skills = skills

			res.send skills

@deleteSkill = (req , res)->
	{type ,id, sid} = req.params

	User_skills.findOne().where("_id"  ,id).exec (err , skills)->
		if err?
			console.log err
			return res.status(500).send err

		skill = skills[type]
		skill = _.without skill , skill[sid]
		skills[type] = skill

		skills.save (err)->
			if err?
				return res.status(500).send err

			if req.user._id == req.body.id
				req.session.skills = skills

			res.send skills

@updateSkill = (req , res) ->
	{type , id , sid} =  req.params
	data = _.omit req.body , ['submit' , '_id' , 'id']

	User_skills.findOne().where("_id" , id).exec (err , skills)->
		if err?
			console.log err
			return res.status(500).send err

		if not skills
			console.log "skill for #{id} not found"
			return res.status(404).send "Not Found"

		skill = skills[type]
		skill[sid] = data
		skills[type] = skill

		skills.save (err)->
			if err?
				console.log err
				return res.status(500).send err

			if req.user._id == req.body.id
				req.session.skills = skills

			res.status(200).send skills
