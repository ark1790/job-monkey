

LocalStrategy = require("passport-local").Strategy

mongoose = require "mongoose"

User = mongoose.model "User"
User_skills = mongoose.model "User_skills"


# Setup function

setupFunction = (passport) ->

	# PASSPORT SESSION SETUP

	passport.serializeUser (user, done) ->
		done null , user.id


	passport.deserializeUser (id , done) ->
		User.findById id , (err , user) ->
			done err , user


	######### LOCAL SIGNUP ################

	passport.use "local-signup" , new LocalStrategy {

		# By default local Strategy uses username and password, we will use override with email

			usernameField : 'email'
			passwordField : 'password'
			passReqToCallback : true  # Allows us to pass the entire request to the callback

		} , (req , email , password , done) ->

			# Check for user
			console.log "YAYAYAYAYAYAYAYAYAYAYAYAY"
			console.log req.files
			User.findOne { 'local.email' : email } , (err , user) ->
				if err? then return done err

				# Check if there is alreay a user

				if user?
					return done null , false , req.flash("signupMessage" , "That email is already taken.")

				# There is no user with that email

				# Create the user

				newUser = new User()

				newUser.local.email = email

				await newUser.generateHash password , defer err , pass

				if err? then return done err

				{ first_name , last_name , birth_date } = req.body

				newUser.local.password = pass
				newUser.role = "basic"
				newUser.first_name = first_name
				newUser.last_name = last_name
				newUser.birth_date = birth_date
				newUser.user_image = req.files.user_image[0].filename


				# Save the User

				newUser.save (err) ->
					if err? then return done err

					done null , newUser

				console.log "ID: "
				console.log newUser.id


	# LOCAL LOGIN

	passport.use "local-login" , new LocalStrategy {

			usernameField : 'email'
			passwordField : 'password'
			passReqToCallback : true

		} , (req , email , password , done) ->

			User.findOne { 'local.email' : email } , (err , user) ->

				if err? then return done err

				unless user?
					return done null , false , req.flash("loginMessage" , 'No user found.')

				# if user is found but the password wrong

				await user.validPassword password , defer err , result

				unless result
					return done null , false , req.flash('loginMessage' , "OOPS! Wrong Password.")

				await User_skills.find {"user_id" : user._id} , defer err , skills

				if err? then console.log err

				req.session.skills = skills

				done null , user # return the callback


module.exports = setupFunction
