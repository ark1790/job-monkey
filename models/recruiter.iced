
mongoose = require "mongoose"

Schema = mongoose.Schema

    name            : String
    phone           : String
    email           : String
    address         : String
    location        : String

    start_date      : Date
    end_date        : Date
    created_by      : mongoose.Schema.Types.ObjectId

    representatives : [{
            id      : mongoose.Schema.Types.ObjectId
            name    : String
        }]

    status          : String

	created_at		: Date


# create time save
Schema.pre 'save' , (next)->
	if not @created_at
		@created_at = Date.now()
	next()




module.exports = mongoose.model "Recruiter" , Schema
