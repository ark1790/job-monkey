

mongoose = require "mongoose"
bcrypt = require "bcrypt-nodejs"


userSchema = mongoose.Schema

	role			: String

	local			:
		email		: String
		password	: String

	username		: String

	first_name		: String
	last_name		: String

	birth_date		: Date

	marked_events	: [{
			id : mongoose.Schema.Types.ObjectId
			name : String
			start_date : Date
			end_date : Date
		}]

	status 			: String
	job_seeker		:
		course				: String
		institution			: String
		preferred_sector	: String
		passing_year		: Date
		preferred_location	:String

	user_image		: String
	created_at		: Date
	updated_at		: Date




# METHODS

# generating a hash

userSchema.methods.generateHash = ( password , next ) ->
	bcrypt.hash password , bcrypt.genSaltSync(8) , null , next


# Checking if a password is valid

userSchema.methods.validPassword  = ( password , next ) ->
	bcrypt.compare password , this.local.password , next


# create/update time save
userSchema.pre 'save' , (next)->
	if not @created_at
		@created_at = Date.now()
		@updated_at = @created_at
	else @updated_at = Date.now()
	next()

userSchema.pre 'update' , (next)->
	@updated_at = Date.now()
	next()

# Create the model for users and expose it to out app

module.exports = mongoose.model "User" , userSchema
