mongoose = require "mongoose"

userSkillsSchema = mongoose.Schema

		education : [
			degree 		: String
			institution : String
			grade 		: String
			duration_from	: String
			duration_to		: String
			_id : false
		]
		
		experience : [
			company		: String
			title		: String
			location	: String
			period_from	: String
			period_to	: String
			details		: String
			_id : false
		]
		
		language : [
			name 		: String
			proficiency : String
			_id : false
		]
		
		
		volunteering : [
			organization	: String
			role			: String
			cause			: String
			period_from		: String
			period_to		: String
			_id : false
		]
		
		
		honors	: [
			title		: String
			occupation	: String
			issuer		: String
			issued_on	: String
			_id : false
		]
		
		courses : [
			name		: String
			association : String
			grade		: String
			completed_on: String 
			_id : false
		]
		
		publications : [
			title 		: String
			publisher	: String
			url 		: String
			authors 	: String
			details		: String
			published_on : String
			_id : false
		]
		
		certifications : [
			name			: String	
			authority		: String	
			license_number	: String	
			url				: String	
			issued_on		: String	
			expiry_date		: String	
		]

		test_scores : [
			name			: String	
			issued_by		: String	
			score			: String	
			issued_on		: String	
		]

		
		user_id : mongoose.Schema.Types.ObjectId
module.exports = mongoose.model "User_skills" , userSkillsSchema
