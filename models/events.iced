
mongoose = require "mongoose"

eventSchema = mongoose.Schema

    name            : String
    details         : String
    location        : String

    start_date      : Date
    end_date        : Date

	created_at		: Date


# create time save
eventSchema.pre 'save' , (next)->
	if not @created_at
		@created_at = Date.now()
	next()




module.exports = mongoose.model "Events" , eventSchema
