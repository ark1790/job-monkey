
mongoose = require "mongoose"


getSalary = (val)->
    return (val/100).toFixed(2)

setSalary = (val)->
    return val*100

Schema = mongoose.Schema

    title           : String
    summary         : String
    details         : String
    qualifications  : String
    skills          : String
    
    location        : String
    job_type        : String
    sector          : String


    job_hours           : String
    salary          : {
        type    : Number
        get     : getSalary
        set     : setSalary
    }
    salary_details  : String

    recruiter       : String

    appl_url        : String
    appl_email      : String
    appl_process    : String

    closing_date    : Date
	created_at		: Date



# create time save
Schema.pre 'save' , (next)->
	if @isNew
		@created_at = Date.now()
	next()


module.exports = mongoose.model "Jobs" , Schema
