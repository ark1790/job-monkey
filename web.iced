
express = require "express"
app = express()
mongoose = require "mongoose"
passport = require "passport"
flash = require "connect-flash"
cookieSession = require 'cookie-session'
morgan = require "morgan"
cookieParser = require "cookie-parser"
bodyParser = require "body-parser"
session = require "express-session"
path = require "path"




require('dotenv').load()


# REQUIRING ALL MODELS IN THE APP

fs  = require "fs"
models_path = __dirname + "/models/"

fs.readdirSync(models_path).forEach (file)->
	require models_path + file

# configuration

mongoose.connect process.env.MONGODB_URL



require("./config/passport")(passport)

# setup express application

app.use morgan('dev')
app.use cookieParser()
app.use bodyParser.json()
app.use bodyParser.urlencoded
	extended : false

app.use express.static( path.join( __dirname, "public"))
app.use express.static( path.join( __dirname, "assets"))
app.use express.static( path.join( __dirname, "uploads"))
app.use express.static( path.join( __dirname, "jango_assets"))


app.set 'view engine' , 'ejs'


# REDIS for session storage

# store = new (require('connect-redis') session)
#   url : process.env.REDISCLOUD_URL
#   ttl: 24*3600

# app.use session
# 	secret : process.env.SECRET
# 	resave : false
# 	saveUninitialized: true
# 	# store: store
# 	key: 's.id'



app.use cookieSession
	name: 's.id'
	keys: (process.env.SECURE_KEY || process.env.SECRET).split /\s+|,/

app.use require('connect-assets')()

# passport settings

app.use passport.initialize()
app.use passport.session()
app.use flash()




require("./routes").bind app

app.get "/advice" , (req , res)->
	res.render "advice",{'current_page': 'advice'}

#
app.get "/find_job" , (req , res)->
	res.render "find_job",{'current_page': 'find_job'}
#
app.get "/recruiters" , (req , res)->
	res.render "recruiters",{'current_page': 'recruiters'}
#
app.get "/events" , (req , res)->
	res.render "events",{'current_page': 'events'}
#
app.get "/blogs" , (req , res)->
	res.render "blogs",{'current_page': 'blogs'}
#
app.get "/sectors" , (req , res)->
	res.render "sectors",{'current_page': 'sectors'}

#
app.get "/login" , (req , res)->
	res.render "login",{'current_page': 'login'}

#
app.get "/calendar" , (req , res)->
	res.render "calendar",{'current_page': 'calendar'}

#
app.get "/inbox" , (req , res)->
	res.render "inbox",{'current_page': 'inbox'}

#
app.get "/recruiter/login" , (req , res)->
	res.render "recruiter_login",{'current_page': 'recruiter login'}

#
app.get "/app_inbox_inbox",(req,res)->
	res.render "app_inbox_inbox"
#
app.get "/app_inbox_view",(req,res)->
	res.render "app_inbox_view"
#
app.get "/app_inbox_compose",(req,res)->
	res.render "app_inbox_compose"
#
app.get "/app_inbox_reply",(req,res)->
	res.render "app_inbox_reply"

#
app.get "/test",(req,res)->
	res.render "index2"



#
app.get "/get_recruiter_search_data" ,(req , res)->
	result = {}
	result.data=[]
	result.data[0]=[]
	result.data[0][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/b6897d3e-45f2-4728-bdd2-b9f3172958c1/'>"
	result.data[0][1]="<a>Accenture</a><br>Accenture is a global management consulting, technology services and outsourcing company, with more than 336,000 people serving clients in more tha..."
	result.data[0][2]="20 Active Jobs/15 Expired"
	result.data[0][3]='<div class="margin-bottom-5"><a href="/find_job" class="btn btn-sm btn-primary margin-bottom"><i class="fa fa-search"></i>See Jobs</a></div><a data-toggle="modal" href="#large" class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i>View Details</a>'
	#

	result.data[1]=[]
	result.data[1][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/077c42a0-f75d-49ec-be5b-ca77ffc53f9a/'>"
	result.data[1][1]="<a>Advanced Computer Software</a><br>The UK’s 2nd-largest software company."
	result.data[1][2]="10 Active Jobs/25 Expired"
	result.data[1][3]='<div class="margin-bottom-5"><a href="/find_job" class="btn btn-sm btn-primary margin-bottom"><i class="fa fa-search"></i>See Jobs</a></div><a data-toggle="modal" href="#large" class="btn btn-sm btn-default filter-cancel"><i class="fa fa-times"></i> View Details</a>'
	#
	result.draw=req.query.draw
	res.send JSON.stringify result

#
app.get "/get_event_search_data" ,(req , res)->
	result = {}
	result.data=[]
	result.data[0]=[]
	result.data[0][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/016ba55a-850f-4390-9b06-d8486e49101f/'>"
	result.data[0][1]="<a>Insight Day – London</a><br>Amazing careers start at Deloitte. Join us at one of our Insight Days and enjoy an unparalleled and inspirational look inside our business, as well as tips on how to ace job interviews and assessment centres."
	result.data[0][2]="Undergraduates / Graduates, Open day"
	result.data[0][3]="Friday, March 18, 2016 1:30pm - Friday, March 18, 2016 5pm"
	result.data[0][4]="Dhaka, Bangladesh"
	result.data[0][5]='<div class="margin-bottom-5"><a href="/find_job" class="btn btn-sm btn-primary margin-bottom"><i class="fa fa-save"></i> Attending</a></div><a data-toggle="modal" href="#large" class="btn btn-sm btn-default filter-cancel"><i class="fa fa-search"></i> View Details</a></div>'
	#

	result.data[1]=[]
	result.data[1][0]="<img style='max-width:100%' src='http://d1a6svqic8kgo7.cloudfront.net/getasset/016ba55a-850f-4390-9b06-d8486e49101f/'>"
	result.data[1][1]="<a>Insight Day – London</a><br>Amazing careers start at Deloitte. Join us at one of our Insight Days and enjoy an unparalleled and inspirational look inside our business, as well as tips on how to ace job interviews and assessment centres."
	result.data[1][2]="Undergraduates / Graduates, Open day"
	result.data[1][3]="Friday, March 18, 2016 1:30pm - Friday, March 18, 2016 5pm"
	result.data[1][4]="Dhaka, Bangladesh"
	result.data[1][5]='<div class="margin-bottom-5"><a href="/find_job" class="btn btn-sm btn-primary margin-bottom"><i class="fa fa-save"></i> Attending</a></div><a data-toggle="modal" href="#large" class="btn btn-sm btn-default filter-cancel"><i class="fa fa-search"></i> View Details</a></div>'
	#
	result.draw=req.query.draw
	res.send JSON.stringify result

app.set 'port' , process.env.PORT || "8080"


server = app.listen app.get('port') , ->
	console.log 'listening on 8080'


module.exports = app
